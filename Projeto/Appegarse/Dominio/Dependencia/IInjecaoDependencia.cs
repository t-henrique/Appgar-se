﻿using System;

namespace Dominio.Dependencia
{
    public interface IInjecaoDependencia
    {
        T Resolve<T>();

        T Resolve<T>(Type type);
    }
}
