﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Models
{
    public class EventosImportantes
    {
        public int EventoId { get; set; }
        public string Descricao { get; set; }
        public DateTime DataAcontecimento { get; set; }
    }
}
