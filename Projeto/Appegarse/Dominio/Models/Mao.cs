﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Models
{
    public class Mao
    {
        public int MaoId { get; set; }
        public int Polegar { get; set; }
        public int Indicador { get; set; }
        public int Medio { get; set; }
        public int Anuliar { get; set; }
        public int Minimo { get; set; }
    }
}
