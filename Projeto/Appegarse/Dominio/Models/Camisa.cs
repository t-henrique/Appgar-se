﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Models
{
    public class Camisa
    {
        public int CamisaId { get; set; }
        public int Ombro { get; set; }
        public int Manga { get; set; }
        public int ToraxBusto { get; set; }
        public int Comprimento { get; set; }
        public int Tamanho { get; set; }
    }
}
