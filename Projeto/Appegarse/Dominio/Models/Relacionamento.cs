﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Models
{
    public class Relacionamento
    {
        public Usuario UsuarioA { get; set; }
        public Usuario UsuarioB { get; set; }
        public List<EventosImportantes> Eventos { get; set; }
    }
}
