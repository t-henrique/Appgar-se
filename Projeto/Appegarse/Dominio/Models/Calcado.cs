﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Models
{
    public class Calcado
    {
        public int CalcadoId { get; set; }
        public EnumTipoCalcado Tipo { get; set; }
        public String Cor { get; set; }
        public String Observacao { get; set; }
        public int TamanhoPe { get; set; }
    }
}
