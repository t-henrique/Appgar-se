﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Models
{
    public class Calca
    {
        public int CalcaId { get; set; }
        public int Cintura { get; set; }
        public int Qadril { get; set; }
        public int Gancho { get; set; }
        public int Comprimento { get; set; }
        public int Tamanho { get; set; }
    }
}
