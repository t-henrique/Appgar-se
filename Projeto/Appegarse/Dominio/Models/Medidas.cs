﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Models
{
    public class Medidas
    {
        public int MedidasId { get; set; }
        public int altura { get; set; }
        public int peso { get; set; }
        public List<Calcado> Calcados { get; set; }
        public Mao Mao { get; set; }
    }
}
