﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Models
{
    public enum EnumTipoCalcado
    {
        Geral = 1,
        Bota = 2,
        Chinelo = 3,
        Sandalia = 4,
        Sapato = 5,
        Tenis = 6
    }
}
