﻿using Dominio.Dependencia;
using System;

namespace InjecaoDependencia
{
    public class Resolvedor
    {
        private static IInjecaoDependencia _injecao;

        public static void Inicializar(IInjecaoDependencia injecao)
        {
            _injecao = injecao;
        }

        public static T Resolver<T>()
        {
            return _injecao.Resolve<T>();
        }

        public static T Resolver<T>(Type type)
        {
            return _injecao.Resolve<T>(type);
        }
    }
}
