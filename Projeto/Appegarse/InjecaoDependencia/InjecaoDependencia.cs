﻿using System;
using Dominio.Dependencia;
using Microsoft.Practices.Unity;

namespace InjecaoDependencia
{
    public class InjecaoDependencia : IInjecaoDependencia
    {
        private readonly IUnityContainer _container;
        private static InjecaoDependencia _injecaoDependencia;

        public InjecaoDependencia()
        {
            _container = new UnityContainer();
            MapearDependencias();
        }

        public static InjecaoDependencia Instancia()
        {
            return _injecaoDependencia ?? (_injecaoDependencia = new InjecaoDependencia());
        }

        public T Resolve<T>()
        {
            return _container.Resolve<T>();
        }

        public T Resolve<T>(Type type)
        {
            return (T)_container.Resolve(type);
        }

        private void MapearDependencias()
        {
            throw new NotImplementedException();
            //_container.RegisterType<IA, A>();
        }

    }
}
