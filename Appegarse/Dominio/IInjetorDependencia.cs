﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public interface IInjetorDependencia
    {
        T Resolve<T>();

        T Resolve<T>(Type type);
    }
}
