﻿using Dominio;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InjecaoDependencias
{
    public class InjetorDependencia : IInjetorDependencia
    {
        private IUnityContainer _container;
        private static InjetorDependencia _injetorDependencia;
        
        private InjetorDependencia()
        {
            _container = new UnityContainer();
            mapearDependencias();
        }

        public static InjetorDependencia Instancia()
        {
            return _injetorDependencia ?? (_injetorDependencia = new InjetorDependencia());
        }

        public T Resolve<T>()
        {
            throw new NotImplementedException();
        }

        public T Resolve<T>(Type type)
        {
            throw new NotImplementedException();
        }

        private void mapearDependencias()
        { 
            _container
                .RegisterType<>();
        }


    }
}
