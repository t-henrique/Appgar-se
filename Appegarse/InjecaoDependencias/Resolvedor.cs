﻿using Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InjecaoDependencias
{
    public class Resolvedor
    {
        private static IInjetorDependencia _injecao;

        public static void Inicializar(IInjetorDependencia injetor)
        {
            _injecao = injetor;
        }

        public static T Resolver<T>()
        {
            return _injecao.Resolve<T>();
        }

        public static T Resolver<T>(Type type)
        {
            return _injecao.Resolve<T>(type);
        }
    }
}
